**AsusGrouperSensorDaemon**


Asus Grouper Nexus 7 sensor daemon is a tool which use iio-sensor-proxy, perl, xinput and xrandr to provide mobile like functionalities to the device while under alpine linux postmarketos (see https://wiki.postmarketos.org/wiki/Main )


**PostmarketOS/Alpine prereqs**


apk add iio-sensor-proxy perl xinput xrandr xset

rc-update add iio-sensor-proxy


**PostmarketOS/Alpine Install instruction:**


git clone https://gitlab.com/gullradriel/asus-grouper-nexus-7-sensor-daemon.git

cd asus-grouper-nexus-7-sensor-daemon

sudo su

cp etc/init.d/* /etc/init.d

cp -r usr/sbin/* /usr/sbin/

chmod +Xx /etc/init.d/asus_grouper_sensors   

chmod +Xx /usr/sbin/asus_grouper_sensor_daemon.pl /usr/sbin/asus_grouper_scripts/*

rc-update add asus_grouper_sensors

/etc/init.d/asus_grouper_sensors start


**Debugging/Testing** 


/etc/init.d/asus_grouper_sensors stop

cd /usr/sbin

./asus_grouper_sensor_daemon.pl VERBOSE

Edit variables in /usr/sbin/asus_grouper_sensor_daemon.pl to adjust sensitivity / timers / disable screen rotation / etc 
