#!/usr/bin/perl -w
#
# Perl daemon to auto adjust brightness , auto rotate , and auto turn on / off screen (disabled until better method is found)
# Based on monitor-sensor output from iio-sensor-proxy
#


use strict;
use warnings;
use threads;
use threads::shared;
use Time::HiRes qw( usleep );

my $monitor_cmd = '/usr/bin/monitor-sensor';         # sensor info command
my $scripts_dir = '/usr/sbin/asus_grouper_scripts' ; # daemon scripts directory
my $min_b : shared = 45.0  ;   # minimum brightness 
my $max_b : shared = 254.0 ;   # maximum brightness value 
my $min_l = 0 ;             # minimum input 
my $max_l = 750.000 ;       # maximum input value (on nexus 7 grouper max mesured:4862.687) it's the lux level at which brightness will be at 100%
my $mul_l = 1.0 ;           # sensor input multiplier, increase sensibility/adjust lux value
my $nb_steps = 10 ;         # number of steps between two transition
my $delay = 33333 ;         # delay in usecs when transiting
my $wait_delay = 250000 ;   # delay in usecs when waiting

my $val : shared = $min_b ;     # default starting value
my $read_lux : shared = 0 ;     # read lux
my $screen_state : shared = 1 ; # starting sreen state 
my $orientation : shared = 'normal' ; # starting orientation 

# regex to capture light value (in lux for N7 but the values are funky)
my $light_capture_pattern = '\s*Light changed: (\d*.\d*) \(.*\)' ;
# regexp to capture orientation mode 
my $orientation_pattern = '\s*Accelerometer orientation changed: (\S*)' ;
# how many time do we need to stay in a new position for rotation to be done ? in usec
my $orientation_change_timer : shared = 750000 ;

#
my $verbose = 0 ;

sub screen_processor {

	local $SIG{KILL} = sub { threads->exit };

	my $b_val = $val ;
	my $b_next = $val ;
	my $screen_state = 1 ;
	my $inc = 0.0 ;
	my $orientation_timer = $orientation_change_timer ; 
	my $last_orientation = 'normal' ;
	my $last_time_step = $wait_delay ; 

	while( 1 )
	{

		if( $screen_state )
		{
			if( $orientation ne $last_orientation )
			{
				$orientation_timer -= $last_time_step ;
				if( $verbose )
				{
					print "orientation timer: $orientation_timer\n" ;
				}
				if( $orientation_timer < 0 )
				{
					# orientation management
					`$scripts_dir/$orientation.ash` ;
					if( $verbose )
					{
						print "Oriention: $orientation\n" ;
					}
					$last_orientation = $orientation ;
					$orientation_timer = $orientation_change_timer ;
				}
			}
			else
			{
				$orientation_timer = $orientation_change_timer ;
			}

			# brightness management
			if( $b_next == $b_val )
			{
				$inc = ( $val - $b_val ) / $nb_steps ;
				if( abs( $inc ) < 1.0 )
				{
					if( $val > $b_val )
					{
						$inc = 1.0 ;
					}
					if( $val < $b_val )
					{
						$inc = -1.0 ;
					}
				}
				$b_next = $val ;
			}
			if( $b_val != $b_next )
			{
				$b_val += $inc ;
				if( abs( $b_val - $b_next ) <= abs( $inc ) )
				{
					$b_val = $b_next ;
				}

				my $brightness = int( $b_val + $min_b );
				if( $brightness > $max_b ) { $brightness = $max_b ; }
				if( $brightness < $min_b ) { $brightness = $min_b ; }
				`echo $brightness | sudo tee /sys/class/backlight/backlight/brightness` ;
				if( $verbose )
				{
					print "Lux: $read_lux => $val Aim: $b_val Current (+off:$min_b): $brightness it: $inc\n";
				}
				$last_time_step = $delay ;
				usleep( $delay );
				next ;
			}
		}
		$last_time_step = $wait_delay ;
		usleep( $wait_delay );
	}
}

#
# START
#

if( $ARGV[ 0 ] )
{
	$verbose = 1 ; 
}

my $MON_OUT ;
my $mon_pid = open( $MON_OUT , "$monitor_cmd |") or die( "could not start $monitor_cmd" );

my $thr = threads->new(\&screen_processor);

sub handle_kill
{
	kill( 9 , $mon_pid );
	close( $MON_OUT );
	$thr->kill('KILL')->detach ;
	#die "Caught a sigterm $!"
	exit 0 ;
};

$SIG{'INT'} = \&handle_kill ;
$SIG{'TERM'} = \&handle_kill ;
$SIG{'USR1'} = \&handle_kill ;

my $line ;
while( $line = <$MON_OUT> )
{
	my $monitor_test_string = `$scripts_dir/screen_state.ash` ;
	my $cmd_state = $? ;

	if( $cmd_state == 1 )
	{
		$screen_state = 0 ;
		next ;
	}
	$screen_state = 1 ;

	if( $line =~ m/$light_capture_pattern/g )
	{
		$read_lux = $1 * $mul_l ;
		if( $read_lux > $max_l )
		{
			$read_lux = $max_l ;
		}
		$val = int( ($read_lux * $max_b ) / $max_l );
		if( $verbose )
		{
			print 'Decoded '.$read_lux.' lux, brightness set to '.$val."\n";
		}
		
		#
		# Attempt to detect that the lid is on/off and set screen power accordingly
		#
		
		if( $read_lux <= $min_l && $screen_state == 1 )
		{
			`$scripts_dir/screen_off.ash` ;
			$screen_state = 0 ;
			if( $verbose )
			{
				print "Screen goes OFF\n" ;
			}
		}
		elsif( $read_lux >= $min_l && $screen_state == 0 )
		{
			`$scripts_dir/screen_on.ash` ;
			$screen_state = 1 ;
			if( $verbose )
			{
				print "Screen goes ON\n" ;
			}
		}

	}
	elsif( $line =~ m/$orientation_pattern/g )
	{	
		# quote the next line to disable orientation changes
		$orientation = $1 ;
	}	
	else
	{
		if( $verbose )
		{
			print "unmatched: $line" ;
		}
	}
}
exit( 0 );
